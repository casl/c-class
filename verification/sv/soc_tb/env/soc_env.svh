//-------------------------------------------------------------------------------------------------- 
// Copyright (c) 2018, IIT Madras All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// - Redistributions of source code must retain the below copyright notice, this list of conditions
//   and the following disclaimer.  
// - Redistributions in binary form must reproduce the above copyright notice, this list of 
//   conditions and the following disclaimer in the documentation and/or other materials provided 
//   with the distribution.  
// - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
//   promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// --------------------------------------------------------------------------------------------------
// Author: J Lavanya
// Email id: lavanya.jagan@gmail.com
// -------------------------------------------------------------------------------------------------
//
class soc_env extends uvm_env;

  `uvm_component_utils_begin(soc_env)
  `uvm_component_utils_end

  riscv_state_agent_config state_agent_cfg;
  riscv_state_agent state_agent;
  riscv_state_scoreboard state_scoreboard;
  soc_env_config env_cfg;

  //soc_reg_functional_coverage m_func_cov_monitor;
  //soc_scoreboard scoreboard;

  // Register layering adapter:

  //reg2apb_adapter reg2apb;

  // Register predictor:
  //uvm_reg_predictor #(apb_seq_item) apb2reg_predictor;

  extern function new(string name = "soc_env", uvm_component parent = null);
  extern function void build_phase(uvm_phase phase);
  extern function void connect_phase(uvm_phase phase);

endclass:soc_env

function soc_env::new(string name = "soc_env", uvm_component parent = null);
  super.new(name, parent);
endfunction

function void soc_env::build_phase(uvm_phase phase);
  if(!uvm_config_db #(soc_env_config)::get(this, "", "soc_env_config", env_cfg)) begin
    `uvm_error("build_phase", "Failed to find soc_env_config")
  end
  if(env_cfg.has_riscv_state_agent) begin
    uvm_config_db #(riscv_state_agent_config)::set(this, "state_agent*", 
                                                  "state_agent_config", env_cfg.state_agent_cfg);
    state_agent = riscv_state_agent::type_id::create("state_agent", this);
  end

//  if(env_cfg.has_soc_functional_coverage) begin
//    m_func_cov_monitor = soc_reg_functional_coverage::type_id::create("m_func_cov_monitor", this);
//  end
  if(env_cfg.has_riscv_state_scoreboard) begin
    state_scoreboard = riscv_state_scoreboard::type_id::create("state_scoreboard", this);
  end
endfunction:build_phase

function void soc_env::connect_phase(uvm_phase phase);
  //if(env_cfg.state_agent_cfg.active == UVM_ACTIVE) begin
    //reg2apb = reg2apb_adapter::type_id::create("reg2apb");
    //
    // Register sequencer layering part:
    //
    // Only set up register sequencer layering if the top level env
    //if(env_cfg.soc_rm.get_parent() == null) begin
    //  env_cfg.soc_rm.APB_map.set_sequencer(state_agent.m_sequencer, reg2apb);
    //end
    //
    // Register prediction part:
    //
    // Replacing implicit register model prediction with explicit prediction
    // based on APB bus activity observed by the APB agent monitor
    // Set the predictor map:
    //apb2reg_predictor.map = env_cfg.soc_rm.APB_map;
    // Set the predictor adapter:
    //apb2reg_predictor.adapter = reg2apb;
    // Disable the register models auto-prediction
    //env_cfg.soc_rm.APB_map.set_auto_predict(0);
    // Connect the predictor to the bus agent monitor analysis port
    //state_agent.ap.connect(apb2reg_predictor.bus_in);
  //end
  //if(env_cfg.has_soc_functional_coverage) begin
  //  state_agent.ap.connect(m_func_cov_monitor.analysis_export);
  //  m_func_cov_monitor.soc_rm = env_cfg.soc_rm;
  //end
  if(env_cfg.has_riscv_state_scoreboard) begin
    state_agent.agent_state_port.connect(state_scoreboard.riscv_state_export);
    state_agent.agent_spike_port.connect(state_scoreboard.spike_state_export);
  end
  // Set up the agent sequencers as resources:
  //uvm_config_db #(apb_sequencer)::set(null, "*", "apb_sequencer", state_agent.m_sequencer);
  //uvm_config_db #(soc_sequencer)::set(null, "*", "soc_sequencer", m_soc_agent.m_sequencer);
endfunction: connect_phase

